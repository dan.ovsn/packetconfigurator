#include "packet_meta_data.h"
#include <QSettings>

const QString c_StartWord = "StartWord";
const QString c_TypeOfPacket = "TypeOfPacket";
const QString c_CheckSum = "CheckSum";
const QString c_SizeOfData = "SizeOfData";
const QString c_Data = "Data";
const QString c_EndWord = "EndWord";

const QString p_sizeBytes = "bytes";
const QString p_name = "name";
const QString p_index = "index";
const QString p_value = "value";
const QString p_enable = "enable";
const QString p_mode = "mode";


PacketMetaData::PacketMetaData(QString id)
    :m_id(id)
{

    m_headerFields << c_StartWord
                   << c_TypeOfPacket
                   << c_CheckSum
                   << c_SizeOfData
                   << c_Data
                   << c_EndWord;
}

QByteArray PacketMetaData::fromSettings(QString fileName)
{
    QSettings settings(fileName, QSettings::IniFormat);
//    settings.beginGroup(c_groupName);
//    int size = settings.beginReadArray("logins");
//    for (int i = 0; i < size; ++i) {
//        settings.setArrayIndex(i);
//        Login login;
//        login.userName = settings.value("userName").toString();
//        login.password = settings.value("password").toString();
//        logins.append(login);
//    }
//    settings.endArray();
//    settings.endGroup();
}

void PacketMetaData::setSettings(QString fileName)
{
    QSettings settings(fileName, QSettings::IniFormat);
    settings.beginReadArray(QString("%1_header")
                            .arg(m_id));

    for (int i = 0; i < m_headerFields.count(); ++i)
    {

        if(m_headerFields[i] == c_StartWord)
        {
            settings.setArrayIndex(i);
            settings.beginGroup(QString("%1")
                                .arg(m_headerFields[i]));
            settings.setValue(p_index, i);
            settings.setValue(p_sizeBytes, 16);
            settings.setValue(p_value,"start_word");
            settings.endGroup();
        }
        else if(m_headerFields[i] == c_TypeOfPacket)
        {
            settings.setArrayIndex(i);
            settings.beginGroup(QString("%1")
                                .arg(m_headerFields[i]));
            settings.setValue(p_index, i);
            settings.setValue(p_sizeBytes, 1);
            settings.endGroup();
        }
        else if(m_headerFields[i] == c_CheckSum)
        {
            settings.setArrayIndex(i);
            settings.beginGroup(QString("%1")
                                .arg(m_headerFields[i]));
            settings.setValue(p_index, i);
            settings.setValue(p_sizeBytes, 4);
            settings.setValue(p_mode, "sha1");
            settings.setValue(p_enable, true);
            settings.endGroup();
        }
        else if(m_headerFields[i] == c_SizeOfData)
        {
            settings.setArrayIndex(i);
            settings.beginGroup(QString("%1")
                                .arg(m_headerFields[i]));
            settings.setValue(p_index, i);
            settings.setValue(p_sizeBytes, 4);
            settings.endGroup();
        }
        else if(m_headerFields[i] == c_Data)
        {
            settings.setArrayIndex(i);
            settings.beginGroup(QString("%1")
                                .arg(m_headerFields[i]));
            settings.setValue(p_index, i);
            settings.endGroup();
        }
        else if(m_headerFields[i] == c_EndWord)
        {
            settings.setArrayIndex(i);
            settings.beginGroup(QString("%1")
                                .arg(m_headerFields[i]));
            settings.setValue(p_index, i);
            settings.setValue(p_sizeBytes, 16);
            settings.setValue(p_value,"end_word");
            settings.endGroup();
        }
    }
    settings.endArray();
}
