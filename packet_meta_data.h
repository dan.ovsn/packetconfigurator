#ifndef PACKETMETADATA_H
#define PACKETMETADATA_H
#include <QByteArray>
#include <QStringList>
#include <QString>

class PacketMetaData
{
public:
    PacketMetaData(QString id);
    QByteArray fromSettings(QString fileName);
    void setSettings(QString fileName);

private:
    QStringList m_headerFields;
    QString m_id;
};

#endif // PACKETMETADATA_H
