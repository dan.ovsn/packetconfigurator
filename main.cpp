#include <QCoreApplication>
#include <QByteArray>
#include <QDataStream>
#include <QIODevice>
#include <QDebug>
#include <QVariant>
#include <QSettings>
#include "packet_meta_data.h"

const quint8 c_sizeOfStr = 14;
enum class PACKAGE_TYPE
{
    ANSWER,
    QUERY
};

const char c_startWord[c_sizeOfStr] = "START_WORD";
const char c_endWord[c_sizeOfStr] = "END_WORD";

const int c_packageType = (int)PACKAGE_TYPE::ANSWER;

quint16 calcDataSize(QByteArray data)
{
    quint16 dataSize = data.count()*2341;
    return dataSize;
}

quint16 calcCheckSum(QByteArray data)
{
    quint16 checkSum = 99;

    return checkSum;
}

QByteArray createHeader(QByteArray data)
{
    if( data.isEmpty())
        return QByteArray();

    quint16 dataSize = 2;
    quint16 checkSum = 1;

    QByteArray ba("");
    ba.append(QVariant(c_startWord).toByteArray());
    ba.append((char*)QVariant(c_packageType).constData(), QVariant(checkSum).Size);
    ba.append((char*)QVariant(checkSum).constData(), QVariant(checkSum).Size);
    ba.append((char*)QVariant(dataSize).constData(), QVariant(checkSum).Size);
    ba.append(QVariant(data).toByteArray());
    ba.append(c_endWord, c_sizeOfStr);

    return ba;
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
//    const char* data = "KillDepth";
//    QByteArray ba(data);
//    ba = createHeader(ba);
////    qDebug() << QString(ba);
    PacketMetaData packet("Server1");
    packet.setSettings("Config.ini");
    return a.exec();
}
